// API: #3 - use axios to make ajax calls
import axios from 'axios';

const ROOT_URL = 'http://reduxblog.herokuapp.com/api';
const API_KEY = '?key=1234567890';

export const FETCH_POSTS = 'fetch_posts';
export const FETCH_POST = 'fetch_post';
export const CREATE_POST = 'create_post';
export const DELETE_POST = 'delete_post';

export function fetchPosts() {
    // API: #4 - make ajax calls using axios and return a promise
    const request = axios.get(`${ROOT_URL}/posts${API_KEY}`);

    return {
        type: FETCH_POSTS,
        // API: #5 - redux-promise will resolve the ajax calls and return the value from the request
        // before it get send to the reducer
        payload: request
    }
}

export function createPost(values, callback) {
    // Execute the function callback after post is created
    const request = axios.post(`${ROOT_URL}/posts${API_KEY}`, values)
        .then(() => callback());

    return {
        type: CREATE_POST,
        payload: request
    }
}

export function fetchPost(id) {
    // API: #4 - make ajax calls using axios and return a promise
    const request = axios.get(`${ROOT_URL}/posts/${id}${API_KEY}`);

    return {
        type: FETCH_POST,
        // API: #5 - redux-promise will resolve the ajax calls and return the value from the request
        // before it get send to the reducer
        payload: request
    }
}

export function deletePost(id, callback) {
    // API: #4 - make ajax calls using axios and return a promise
    const request = axios.delete(`${ROOT_URL}/posts/${id}${API_KEY}`)
        .then(() => callback());

    return {
        type: DELETE_POST,
        // API: #5 - redux-promise will resolve the ajax calls and return the value from the request
        // before it get send to the reducer
        payload: id
    }

}