import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createPost } from '../actions';

class PostsNew extends Component {

    renderTextField(field) {
        const { meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? 'has-danger' : '' }`;
        return (
            <div className={className}>
                <label>{field.label}</label>
                <input 
                    className="form-control"
                    type="text"
                    {...field.input}
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        );
    }

    onSubmit(values) {
        // use callback to programmatically set new route 
        // after completing creation of post
        this.props.createPost(values, () => this.props.history.push('/'));
    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <Field 
                    label="Title for Post"
                    name="title"
                    component={this.renderTextField}
                    />
                <Field 
                    label="Categories"
                    name="categories"
                    component={this.renderTextField}
                    />
                <Field 
                    label="Post Content"
                    name="content"
                    component={this.renderTextField}
                    />
                <button type="submit" className="btn btn-primary">Submit</button>
                <Link to="/" className="btn btn-danger">Cancel</Link>
            </form>
        );
    }
}

// helper function to validate values before submitting the form
function validate(values) {
    // console.log(values) -> { title: 'Hello', categories: 'javascript', content: 'nodejs is awesome' }
    const errors = {};

    // validate the inputs from 'values'
    if (!values.title) {
        errors.title = "Enter a title";
    }
    if (!values.categories) {
        errors.categories = "Enter some categories";
    }
    if (!values.content) {
        errors.content = "Enter some content please";
    }

    // If errors is empty, the form is fine to submit
    // If errors has *any* properties, redux form assumes form is invalid
    return errors;
}

export default reduxForm({
    form: 'PostsNewForm',
    validate
})(
    connect(null, { createPost })(PostsNew)
);