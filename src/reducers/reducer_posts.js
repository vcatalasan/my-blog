import _ from 'lodash';
import { FETCH_POSTS, FETCH_POST, DELETE_POST } from '../actions';

export default function(state = {}, action) {
    switch (action.type) {

        case DELETE_POST:
            return _.omit(state, action.payload);

        case FETCH_POSTS:
            console.log(action.payload.data);  // [ post1, post2 ]

            // convert posts array into an object { 4: post } with id field as key using lodash
            return _.mapKeys(action.payload.data, 'id');

        case FETCH_POST:
            //ES5 javascript version
            // const post = action.payload.data;
            // const newState = { ...state };
            // newState[post.id] = post;
            // return newState;
            
            //ES6 version using destructuring
            return { ...state, [action.payload.data.id]: action.payload.data };
        
        default:
            return state;
    }
}