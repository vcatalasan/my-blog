import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
// API: #1 - use redux promise middleware to resolve async calls from action creators
import promise from 'redux-promise';

import reducers from './reducers';
import PostsIndex from './components/posts_index';
import PostsNew from './components/posts_new';
import PostsShow from './components/posts_show';

// API: #2 - hook promise middleware to redux store
const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

// link redux store with provider and use switch to show the component that
// matches the first route's path
ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router>
      <Switch>
        <Route path="/posts/new" component={PostsNew} />
        <Route path="/posts/:id" component={PostsShow} />
        <Route path="/" component={PostsIndex} />
      </Switch>
    </Router>
  </Provider>
  , document.querySelector('.container'));
